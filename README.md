# About CustomColors

*CustomColors* is a [0 A.D.](https://play0ad.com) mod that allows you to replace default player colors with user-defined colors.

# Features

* Global color replacement: custom colors will be displayed everywhere in the game.
* Replay files are not affected: other players will see the original colors if you exchange your replays with them.

# Installation

[Click here](https://gitlab.com/mentula0ad/CustomColors/-/releases/permalink/latest/downloads/customcolors.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/CustomColors/-/releases/permalink/latest/downloads/customcolors.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/CustomColors/-/releases/permalink/latest/downloads/customcolors.zip) | [Older Releases](https://gitlab.com/mentula0ad/CustomColors/-/releases)
