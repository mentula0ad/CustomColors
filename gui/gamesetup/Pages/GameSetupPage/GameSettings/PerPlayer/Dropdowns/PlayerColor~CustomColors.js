class PlayerColor_CustomColors extends PlayerSettingControls.PlayerColor {

    constructor(...args)
    {
        super(...args);
    }

    render(...args)
    {
        super.render(...args);

        if (!global.g_CustomColors)
            return;

        if (g_GameSettings.playerCount.nbPlayers < this.playerIndex + 1)
	    return;

        // Change backgroud sprite
        this.playerBackgroundColor.sprite = "color:" + rgbToGuiColor(g_CustomColors.substitute(g_GameSettings.playerColor.get(this.playerIndex)), 100);

        // Change dropdown icons
        this.dropdown.list = this.values.map(x => coloredText(this.ColorIcon, rgbToGuiColor(g_CustomColors.substitute(x))));
    }

}

PlayerSettingControls.PlayerColor = PlayerColor_CustomColors;
