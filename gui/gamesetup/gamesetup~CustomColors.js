var g_CustomColors;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    g_CustomColors = new CustomColors();

    // Refresh GUI
    for (let i=0; i<g_MaxPlayers; i++)
        g_SetupWindow.pages.GameSetupPage.gameSettingControlManager.playerSettingControlManagers[i].playerSettingControls.PlayerColor.render();
}});
