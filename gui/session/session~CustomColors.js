var g_CustomColors;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    g_CustomColors = new CustomColors();
    g_CustomColors.updateGUI();
}});

updatePlayerData = new Proxy(updatePlayerData, {apply: function(target, thisArg, args) {
    target(...args);

    if (!global.g_CustomColors)
        return;

    g_CustomColors.updatePlayerData();
}});
