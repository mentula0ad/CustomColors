init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    // Save default settings if they don't exist
    const settings = new CustomColorsSettings();
    settings.createDefaultSettingsIfNotExist();

    // Color labels
    let options = Engine.ReadJSONFile("gui/options/options~CustomColors.json");
    options[0].options.forEach((x, i) => x.label = coloredText(x.label, x.val));
    g_Options = g_Options.concat(options);

    // TODO: translation. The expected command (below) raises an error
    // translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions
    );
}});
