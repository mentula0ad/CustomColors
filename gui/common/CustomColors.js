class CustomColors
{

    constructor()
    {
        this.init();

        this.defaultColors = this.getDefaultColors();
        this.replacedColors = this.getReplacedColors();
        registerConfigChangeHandler(this.onConfigChange.bind(this));
    }

    init()
    {
        // Save default settings if they don't exist
        const settings = new CustomColorsSettings();
        settings.createDefaultSettingsIfNotExist();
    }

    getDefaultColors()
    {
        const settings = new CustomColorsSettings();
        const defaultSettings = settings.trim(settings.getDefault());
        // Here we are assuming that there's a 1-1 correspondence between settings keys and colors
        let ret = new Array(Object.keys(defaultSettings).length);
        Object.keys(defaultSettings).forEach(x => ret[+x-1] = guiToRgbColor(defaultSettings[x]));
        return ret;
    }

    getReplacedColors()
    {
        const settings = new CustomColorsSettings();
        const savedSettings = settings.trim(settings.getSaved());
        // Here we are assuming that there's a 1-1 correspondence between settings keys and colors
        let ret = new Array(Object.keys(savedSettings).length);
        Object.keys(savedSettings).forEach(x => ret[+x-1] = guiToRgbColor(savedSettings[x]));
        return ret;
    }

    substitute(color)
    {
        const index = this.defaultColors.findIndex(x => sameColor(x, color));
        if (index < 0)
            return color;
        return this.replacedColors[index];
    }

    updateGPlayers()
    {
        g_Players.forEach((x, i) => g_Players[i].color = this.substitute(x.color));
    }

    updateGGameData()
    {
        g_GameData.sim.playerStates.forEach((x, i) => g_GameData.sim.playerStates[i].color = rgbByteToDecimal(this.substitute(rgbDecimalToByte(x.color))));
    }

    updateGUI()
    {
        this.updateGPlayers();
        g_DiplomacyColors.updateDisplayedPlayerColors();
        g_PlayerViewControl.rebuild();
    }

    updatePlayerData()
    {
        this.defaultColors = this.getDefaultColors();
        g_CustomColors.updateGPlayers();
    }

    onConfigChange(changes)
    {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("customcolors.")))
        {
            this.defaultColors = [...this.replacedColors];
            this.replacedColors = this.getReplacedColors();
            if (global.g_Players)
                this.updateGUI();
        }
    }
}
